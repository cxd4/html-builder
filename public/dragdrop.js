document.addEventListener("keydown", function(event) {
    switch (event.key) {
    case "Backspace":
        remove_last();
        break;
    }
});

var coords_per_vtx = 2;
var circle_precision = 180;
var arc_120deg = circle_precision / 3;
var channels = 4;

var index_buffer = [
    0 * arc_120deg + circle_precision / 4 + 1,
    1 * arc_120deg + circle_precision / 4 + 1,
    2 * arc_120deg + circle_precision / 4 + 1
];
function init() {
    var circle = [];
    var colors = [1, 1, 1, 1];
    var i, j;
    var radius = 1.0;

    circle[0] = circle[1] = 0.0;
    if (coords_per_vtx > 2) {
        circle[0 * coords_per_vtx + 2] = 0;
    }
    if (coords_per_vtx > 3) {
        circle[0 * coords_per_vtx + 3] = 1.0 / radius;
    }

    for (i = 0 + 1; i < circle_precision + 1; i += 1) {
        var x, y, z;
        var degs, rads;

        degs = i * (360 / circle_precision);
        rads = degs * (Math.PI / 180.0);
        x = Math.cos(rads) / radius;
        y = Math.sin(rads) / radius;
        z = Math.sqrt(Math.abs(radius * radius - x * x - y * y));

        circle[coords_per_vtx * i + 0] = x;
        circle[coords_per_vtx * i + 1] = y;
        if (coords_per_vtx > 2) {
            circle[coords_per_vtx * i + 2] = z;
        }
        if (coords_per_vtx > 3) {
            circle[coords_per_vtx * i + 3] = 1.0 / radius;
        }
    }

    for (j = 0; j < coords_per_vtx; j += 1) {
        circle[coords_per_vtx * i + j] = circle[coords_per_vtx * 1 + j];
    }
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(coords_per_vtx, GL_FLOAT, 0, circle);

    for (i = 0; i < arc_120deg; i += 1) {
        j = index_buffer[0] + i;
        colors[channels * j + 0] = 1.0 - (i / arc_120deg);
        colors[channels * j + 1] = 0.0;
        colors[channels * j + 2] = 0.0 + (i / arc_120deg);
    }
    for (i = 0; i < arc_120deg; i += 1) {
        j = index_buffer[1] + i;
        colors[channels * j + 0] = 0.0;
        colors[channels * j + 1] = 0.0 + (i / arc_120deg);
        colors[channels * j + 2] = 1.0 - (i / arc_120deg);
    }
    for (i = 0; i < arc_120deg; i += 1) {
        j = index_buffer[2] + i;
        if (j >= circle_precision + 1) {
            j -= circle_precision;
        } /* Fix wraparound into a circular buffer. */
        colors[channels * j + 0] = 0.0 + (i / arc_120deg);
        colors[channels * j + 1] = 1.0 - (i / arc_120deg);
        colors[channels * j + 2] = 0.0;
    }

    for (j = 0; j < channels; j += 1) {
        colors[channels * (circle_precision + 1) + j] = colors[channels + j];
    }
    if (channels > 3) {
        for (i = 1; i < circle_precision + 1 + 1; i += 1) {
            colors[channels * i + 3] = 1.00;
        }
    }

    glVertexPointer(coords_per_vtx, GL_FLOAT, 0, circle);
    glColorPointer(channels, GL_FLOAT, 0, colors);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    return;
}

function display() {
    glDrawArrays(GL_TRIANGLE_FAN, 0, circle_precision + 1 + 1);
    glFinish();
    return;
}

function glclick(event) {
    var old_HTML;
    var new_HTML;
    var x1;
    var x2;
    var pixels = new Uint8Array(4);
    var cx = parseInt(GL_canvas.getBoundingClientRect().left);
    var cy = parseInt(GL_canvas.getBoundingClientRect().top);
    var x = event.clientX - cx;
    var y = 256 - (event.clientY - cy);

    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    display();

//    glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    GL.readPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    old_HTML = frame.getAttribute("srcdoc");
    x1 = old_HTML.indexOf("<body");
    x2 = old_HTML.indexOf(">", x1) + 1;

    new_HTML =
        old_HTML.substring(0, x1) +
        "<body style='background-color:rgb(" +
        pixels[0] + ", " + pixels[1] + ", " + pixels[2] +
        ")'>" +
        old_HTML.substring(x2)
    ;
    frame.setAttribute("srcdoc", new_HTML);
    return;
}

function main_GL() {
    "use strict";
    var error_code;

    if (GL_get_context(document, "GL_canvas") === null) {
        window.alert("Failed to initialize WebGL.");
        return;
    }

    init();
    setInterval(display, 1000 / 3);
    return;
}

/*
 * Define variables for the values computed by
 * the grabber event handler but needed by mover
 * event handler
 */
var diffX, diffY;

var source_element;
var cloned_element;

var prev_x;
var prev_y;

function grabber(event) {
    /* Set the global variable for the element to be moved. */
    source_element = event.currentTarget;
    cloned_element = source_element.cloneNode(true);
    cloned_element.style.position = "absolute";
    source_element.after(cloned_element);
    /*
     * Determine the position of the word to be grabbed,
     * first removing the units from left and top.
     */
    var posX =
        cloned_element.getBoundingClientRect().left -
        cloned_element.offsetWidth +
        window.scrollX
    ;
    var posY =
        cloned_element.getBoundingClientRect().top +
        window.scrollY
    ;
    cloned_element.style.left = posX + "px";
    cloned_element.style.top = posY + "px";

    prev_x = posX;
    prev_y = posY;

    /*
     * Compute the difference between where it is and
     * where the mouse click occurred.
     */
    diffX = event.clientX - posX;
    diffY = event.clientY - posY;

    /*
     * Now register the event handlers for moving and
     * dropping the word.
     */
    document.addEventListener("mousemove", mover, true);
    document.addEventListener("mouseup", dropper, true);

    /*
     * Stop propagation of the event and stop any default
     * browser action.
     */
    event.stopPropagation();
    event.preventDefault();
}

function mover(event) {
/* Compute the new position, add the units, and move the word. */
    cloned_element.style.left = (event.clientX - diffX) + "px";
    cloned_element.style.top = (event.clientY - diffY) + "px";

/* Prevent propagation of the event. */
    event.stopPropagation();
}

function remove_last() {
    var x1;
    var x2;
    var to_remove;
    var key = "<span id='cursor'>_</span></body>";
    var new_HTML;
    var old_HTML = frame.getAttribute("srcdoc").split(key, 2);

    x2 = old_HTML[0].search(/<\/\w+>$/);
    if (x2 < 0) {
        return;
    }
    to_remove = "<" + old_HTML[0].substring(x2 + 2);
    x1 = old_HTML[0].lastIndexOf(to_remove);

    new_HTML = old_HTML[0].substring(0, x1) + key + old_HTML[1];
    frame.setAttribute("srcdoc", new_HTML);
}

function dropper(event) {
    var old_HTML;
    var new_HTML;
    var user_list;
    var user_desc;
    var user_string = "";
    //var key = "</body>";
    var key = "<span id='cursor'>_</span></body>";

    /* Unregister the event handlers for mouseup and mousemove. */
    document.removeEventListener("mouseup", dropper, true);
    document.removeEventListener("mousemove", mover, true);
    event.stopPropagation();

    old_HTML = frame.getAttribute("srcdoc").split(key, 2);
    switch (cloned_element.id) {
    case "h1":
    case "h2":
    case "h3":
    case "h4":
    case "h5":
    case "h6":
    case "h7":
        user_string = window.prompt("Insert sized heading under what label?");
        if (!user_string)
            break;
        user_string =
            "<" + cloned_element.id + ">" +
            user_string +
            "</" + cloned_element.id + ">";
        break;
    case "b":
        user_string = window.prompt("Bold what text?");
        if (!user_string)
            break;
        user_string = "<b>" + user_string + "</b>";
        break;
    case "i":
        user_string = window.prompt("Italicize what text?");
        if (!user_string)
            break;
        user_string = "<bi" + user_string + "</i>";
        break;
    case "u":
        user_string = window.prompt("Underline what text?");
        if (!user_string)
            break;
        user_string = "<u>" + user_string + "</u>";
        break;
    case "kbd":
        user_string = window.prompt("Enter text for monospace/typewriter?");
        if (!user_string)
            break;
        user_string = "<kbd>" + user_string + "</kbd>";
        break;
    case "p":
        user_string = window.prompt("Please enter a paragraph:");
        if (!user_string)
            break;
        user_string = "<p>" + user_string + "</p>";
        break;
    case "blockquote":
        user_string = window.prompt("Cite what quotation onto the page?");
        if (!user_string)
            break;
        user_string = "<blockquote>" + user_string + "</blockquote>";
        break;
    case "hr":
        user_string = "<hr>";
        break;
    case "br":
        user_string = "<br>";
        break;
    case "div":
        user_string = window.prompt("Divide into new section from what text?");
        if (!user_string)
            break;
        user_string = "<div>" + user_string + "</div>";
        break;
    case "span":
        user_string = window.prompt("Generic text span from what message?");
        if (!user_string)
            break;
        user_string = "<span>" + user_string + "</span>";
        break;
    case "ol":
    case "ul":
        user_string = "<" + cloned_element.id + ">";
        while (true) {
            user_list = window.prompt("Enter next item in new list:");
            if (!user_list) {
                break;
            }
            user_string += "<li>" + user_list + "</li>";
        }
        if (user_string === "<" + cloned_element.id + ">") {
            break;
        }
        user_string += "</" + cloned_element.id + ">";
        break;
    case "dl":
        user_string = "<dl>";
        while (true) {
            user_list = window.prompt("Start by defining what term?");
            user_desc = window.prompt("Provide a description of the term?");
            if (!user_list && !user_desc) {
                break;
            }
            user_string +=
                "<dt>" + user_list + "</dt>" +
                "<dd>" + user_desc + "</dd>"
            ;
        }
        if (user_string === "<dl>") {
            break;
        }
        user_string += "</dl>";
        break;
    }
    new_HTML = old_HTML[0] + user_string + key + old_HTML[1];
    frame.setAttribute("srcdoc", new_HTML);
    cloned_element.remove();
}
